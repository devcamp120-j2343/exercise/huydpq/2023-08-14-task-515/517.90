import { Component } from "react";

class TimeClick extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dateTime: [],
        }
    }

    addListClick = () => {
        var time = new Date().toLocaleString('en-GB', {
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: true
        })
        this.setState({
            dateTime: [...this.state.dateTime,time]
        })
    }

    render() {
        return (
            <>
                <h2> List</h2>
                <ol>
                    {
                        this.state.dateTime.map(function (element, index) {
                            return (
                                <li key={index}> {element}</li>
                            )
                        })
                    }
                </ol>
                <button onClick={this.addListClick}>Add list</button>
            </>
        )
    }
}

export default TimeClick